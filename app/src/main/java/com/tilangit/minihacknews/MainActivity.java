package com.tilangit.minihacknews;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.tilangit.minihacknews.adapter.RecyclerViewAdapter;
import com.tilangit.minihacknews.api.APIInterface;
import com.tilangit.minihacknews.api.responsemodel.DynamicResponse;
import com.tilangit.minihacknews.api.service.ServiceGenerator;
import com.tilangit.minihacknews.model.Story;
import com.tilangit.minihacknews.model.StoryIds;
import com.tilangit.minihacknews.utils.Contract;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();


    @BindView(R.id.fav_title) TextView mFavTitle;
    @BindView(R.id.fav_label) TextView mFavLabel;
    @BindView(R.id.recycler_view) RecyclerView mRecylcerView;
    @BindView(R.id.progress_bar) ProgressBar mProgressBar;

    private List<Story> mStoryList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setTitle("Top Stories");
        initRecyler();
        initTopStoryRequest();

        checkingHashMap();
    }

    private void checkingHashMap() {
        if (!Contract.BOOKMARKED.isEmpty()) {
            Map.Entry<String, String> entry = Contract.BOOKMARKED.entrySet().iterator().next();
            String key = entry.getKey();
            String value = entry.getValue();
            mFavTitle.setText(value);
            mFavTitle.setVisibility(View.VISIBLE);
            mFavLabel.setVisibility(View.VISIBLE);
        }else {
            mFavLabel.setVisibility(View.GONE);
            mFavTitle.setVisibility(View.GONE);
        }
    }

    private void showProgressBar() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    private void hideProgressBar() {
        mProgressBar.setVisibility(View.GONE);
    }

    private void initRecyler() {
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2);
        RecyclerViewAdapter recyclerViewAdapter = new RecyclerViewAdapter(this, mStoryList);
        mRecylcerView.setLayoutManager(gridLayoutManager);
        mRecylcerView.setAdapter(recyclerViewAdapter);

        hideProgressBar();

        recyclerViewAdapter.setOnItemClickListener(new RecyclerViewAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, Story story, int position) {
                Intent intent = new Intent(MainActivity.this, DetailStoryActivity.class);
                intent.putExtra(Contract.KEY_ID, story.getId());
                intent.putExtra(Contract.KEY_KIDS, story.getKids());
                startActivity(intent);
            }
        });
    }

    private void initTopStoryRequest() {
        showProgressBar();
        ServiceGenerator.createService(APIInterface.class).getTopStory().enqueue(new Callback<List<String>>() {
            @Override
            public void onResponse(Call<List<String>> call, Response<List<String>> response) {
                List<String> reqResponse = response.body();
                for (int i = 0; i < reqResponse.size(); i++) {
//                    Log.d(TAG, "onResponse: "+reqResponse.get(i));
                    initStoryRequest(reqResponse.get(i));
                }
            }

            @Override
            public void onFailure(Call<List<String>> call, Throwable t) {

            }
        });
    }

    private void initStoryRequest(String id) {
        ServiceGenerator.createService(APIInterface.class).getStory(id).enqueue(new Callback<Story>() {
            @Override
            public void onResponse(Call<Story> call, Response<Story> response) {
                if (response.isSuccessful()) {
                    mStoryList.add(response.body());
                    initRecyler();

                }

            }

            @Override
            public void onFailure(Call<Story> call, Throwable t) {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkingHashMap();
    }
}
