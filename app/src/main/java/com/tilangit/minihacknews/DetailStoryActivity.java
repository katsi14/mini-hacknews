package com.tilangit.minihacknews;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toolbar;

import com.tilangit.minihacknews.adapter.RecyclerViewCommentAdapter;
import com.tilangit.minihacknews.api.APIInterface;
import com.tilangit.minihacknews.api.service.ServiceGenerator;
import com.tilangit.minihacknews.model.Comment;
import com.tilangit.minihacknews.model.Story;
import com.tilangit.minihacknews.utils.Contract;
import com.tilangit.minihacknews.utils.Tools;

import java.util.ArrayList;
import java.util.List;

public class DetailStoryActivity extends AppCompatActivity {
    private static final String TAG = DetailStoryActivity.class.getSimpleName();

    @BindView(R.id.recycler_view) RecyclerView mRecyclerView;
    @BindView(R.id.story_title) TextView mStoryTitle;
    @BindView(R.id.creator_label) TextView mCreator;
    @BindView(R.id.description_text) TextView mDescription;
    @BindView(R.id.time_label) TextView mTime;
    @BindView(R.id.bookmark_btn) TextView mBookmarkBtn;

    List<Comment> mCommentList = new ArrayList<>();
    private String mStoryId = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_story);
        ButterKnife.bind(this);
        setTitle("Story Detail");

        savedInstanceState = this.getIntent().getExtras();
        if (savedInstanceState != null) {
            String id = getIntent().getStringExtra(Contract.KEY_ID);
            int[] cloneKids = getIntent().getIntArrayExtra(Contract.KEY_KIDS).clone();
            for (int cloneKid : cloneKids) {
                initCommentRequest(String.valueOf(cloneKid));
            }
            initRecyler();
            initStoryRequest(id);
        }

        mBookmarkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(DetailStoryActivity.this);
                alertDialog.setMessage("Story Bookmarked").setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Contract.BOOKMARKED.put(mStoryId, mStoryTitle.getText().toString());
                        startActivity(new Intent(DetailStoryActivity.this, MainActivity.class));
                        finish();
                    }
                }).show();
                }
        });
    }

    private void initRecyler() {
        RecyclerViewCommentAdapter recyclerViewCommentAdapter = new RecyclerViewCommentAdapter(this, mCommentList);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(recyclerViewCommentAdapter);
    }


    private void initStoryRequest(String id) {
        ServiceGenerator.createService(APIInterface.class).getStory(id).enqueue(new Callback<Story>() {
            @Override
            public void onResponse(Call<Story> call, Response<Story> response) {
                if (response.isSuccessful()) {
                    Story story = response.body();
                    mStoryTitle.setText(story.getTitle());
                    mCreator.setText(String.format("Creator: %s", story.getBy()));
                    mTime.setText(Tools.getFullDate(story.getTime()));
                    mDescription.setText(story.getUrl());

                }

            }

            @Override
            public void onFailure(Call<Story> call, Throwable t) {

            }
        });
    }

    private void initCommentRequest(String id) {
        ServiceGenerator.createService(APIInterface.class).getComment(id).enqueue(new Callback<Comment>() {
            @Override
            public void onResponse(Call<Comment> call, Response<Comment> response) {
                if (response.isSuccessful()) {
                    Comment comment = response.body();
                    mCommentList.add(comment);
                }

            }

            @Override
            public void onFailure(Call<Comment> call, Throwable t) {

                Log.d(TAG, "onFailure: " + t.getMessage());
            }
        });
    }
}
