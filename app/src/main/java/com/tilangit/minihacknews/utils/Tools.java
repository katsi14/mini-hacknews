package com.tilangit.minihacknews.utils;

import org.joda.time.DateTime;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class Tools {

    public static String getFullDate(int timestamp) {
        Calendar mydate = Calendar.getInstance();
        mydate.setTimeInMillis(timestamp*1000L);
        return mydate.get(Calendar.DAY_OF_MONTH) + "/" + mydate.get(Calendar.MONTH) + "/" + mydate.get(Calendar.YEAR);
    }

    public static String timestampToDate(long timestamp) {
        Date date = new Date(timestamp * 1000L);
        SimpleDateFormat sdf = new SimpleDateFormat(Contract.MY_DATE_FORMAT, Locale.getDefault());
        return sdf.format(date);
    }


}
