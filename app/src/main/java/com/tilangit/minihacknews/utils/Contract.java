package com.tilangit.minihacknews.utils;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class Contract {
    public static final String KEY_ID = "id";
    public static final String KEY_KIDS = "kids";
    public static final String KEY_TITLE = "title";

    public static String MY_DATE_FORMAT = "dd/MM/yyyy";
    public static Map<String, String> BOOKMARKED = new LinkedHashMap<>();

}
