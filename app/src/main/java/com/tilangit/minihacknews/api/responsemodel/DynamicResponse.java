package com.tilangit.minihacknews.api.responsemodel;

import java.util.List;

public class DynamicResponse<T3>{
    List<T3> data;


    public List<T3> getData() {
        return data;
    }

    public void setData(List<T3> data) {
        this.data = data;
    }
}
