package com.tilangit.minihacknews.api.responsemodel;

public class BasicResponse {
    int status;
    String message;

    public BasicResponse() {
    }

    public BasicResponse(int status, String message) {
        this.status = status;
        this.message = message;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
