package com.tilangit.minihacknews.adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tilangit.minihacknews.R;
import com.tilangit.minihacknews.model.Comment;
import com.tilangit.minihacknews.model.Story;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerViewCommentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private List<Comment> mList;
    private OnItemClickListener mOnItemClickListener;

    public RecyclerViewCommentAdapter(Context mContext, List<Comment> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, Comment comment, int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_comments, parent, false);
        vh = new OriginalViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        OriginalViewHolder viewHolder = (OriginalViewHolder) holder;
        Comment comment = mList.get(position);
        final int pos = position;
        viewHolder.lyt_parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(v, mList.get(pos), pos);
                }
            }
        });
        viewHolder.comment.setText(Html.fromHtml(comment.getText()));
        viewHolder.user.setText(comment.getBy());
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }


    public class OriginalViewHolder extends RecyclerView.ViewHolder {
        TextView user,comment;
        View lyt_parent;

        OriginalViewHolder(View v) {
            super(v);
            user = v.findViewById(R.id.username);
            comment = v.findViewById(R.id.comment_text);
            lyt_parent = (View) v.findViewById(R.id.lyt_parent);
        }
    }
}
