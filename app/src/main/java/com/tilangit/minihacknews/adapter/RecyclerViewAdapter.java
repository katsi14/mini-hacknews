package com.tilangit.minihacknews.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tilangit.minihacknews.R;
import com.tilangit.minihacknews.model.Story;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private List<Story> mList;
    private OnItemClickListener mOnItemClickListener;

    public RecyclerViewAdapter(Context mContext, List<Story> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    public interface OnItemClickListener {
        void onItemClick(View view, Story story, int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_story, parent, false);
        vh = new OriginalViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        OriginalViewHolder viewHolder = (OriginalViewHolder) holder;
        Story story = mList.get(position);
        final int pos = position;
        viewHolder.lyt_parent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnItemClickListener != null) {
                    mOnItemClickListener.onItemClick(v, mList.get(pos), pos);
                }
            }
        });

        viewHolder.title.setText(story.getTitle());
        viewHolder.score.setText(String.valueOf(story.getScore()));
        viewHolder.commentCounts.setText(String.valueOf(story.getDescendants()));
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }


    public class OriginalViewHolder extends RecyclerView.ViewHolder {
        TextView title,score, commentCounts;
        View lyt_parent;

        OriginalViewHolder(View v) {
            super(v);
            title = v.findViewById(R.id.story_title);
            score = v.findViewById(R.id.score_value);
            commentCounts = v.findViewById(R.id.comment_counts);
            lyt_parent = (View) v.findViewById(R.id.lyt_parent);
        }
    }
}
