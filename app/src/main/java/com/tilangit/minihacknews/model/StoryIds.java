package com.tilangit.minihacknews.model;

public class StoryIds {
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
