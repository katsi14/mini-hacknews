package com.tilangit.minihacknews.api;

import com.tilangit.minihacknews.api.responsemodel.DynamicResponse;
import com.tilangit.minihacknews.model.Comment;
import com.tilangit.minihacknews.model.Story;
import com.tilangit.minihacknews.model.StoryIds;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface APIInterface {

    @GET("topstories.json")
    Call<List<String>> getTopStory();

    @GET("item/{id}.json")
    Call<Story> getStory(@Path("id") String id);

    @GET("item/{id}.json")
    Call<Comment> getComment(@Path("id") String id);

}
